package com.kevinhao426.uiTest

import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.kevinhao426.channelninecodetest.model.NewsModel
import com.kevinhao426.channelninecodetest.view.MainActivity
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityUITest {

    @get:Rule
    var activityScenarioRule: ActivityScenarioRule<MainActivity> =
        ActivityScenarioRule(MainActivity::class.java)

    lateinit var scnario: ActivityScenario<MainActivity>

    @Before
    fun initTest() {
        scnario = activityScenarioRule.scenario
    }

    // Test whether the alert dialog is displayed while error received
    @Test
    fun testOnErrorEvent() {
        scnario.onActivity { activity ->
            activity.viewModel.errorData.postValue("test error body")
        }

        Espresso.onView(ViewMatchers.withText("test error body"))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    // test whether the tool bar title will change after the news updated
    @Test
    fun testTitleBarUpdation() {
        scnario.moveToState(Lifecycle.State.RESUMED)

        scnario.onActivity { activity ->
            activity.viewModel.newsData.postValue(
                NewsModel(listOf(), "title", listOf(), 0L)
            )
        }

        Espresso.onView(
            Matchers.allOf(
                ViewMatchers.isAssignableFrom(TextView::class.java),
                ViewMatchers.withParent(ViewMatchers.isAssignableFrom(Toolbar::class.java))
            )
        ).check(ViewAssertions.matches(ViewMatchers.withText("title")))
    }


}