package com.kevinhao426.channelninecodetest.api

import com.kevinhao426.channelninecodetest.model.NewsModel
import retrofit2.Response

interface ApiHelper {
    suspend fun getChannelNineNews(): Response<NewsModel>
}