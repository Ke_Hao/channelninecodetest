package com.kevinhao426.channelninecodetest.api

import com.kevinhao426.channelninecodetest.model.NewsModel
import retrofit2.Response
import javax.inject.Inject

class ApiHelperImpl @Inject constructor(
    private val apiService: ApiService
) : ApiHelper {
    override suspend fun getChannelNineNews(): Response<NewsModel> = apiService.getChannelNineNews()
}