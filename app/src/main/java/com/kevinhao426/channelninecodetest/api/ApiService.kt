package com.kevinhao426.channelninecodetest.api

import com.kevinhao426.channelninecodetest.model.NewsModel
import retrofit2.Response
import retrofit2.http.GET

interface ApiService{
    @GET("1/coding_test/13ZZQX/full")
    suspend fun getChannelNineNews(): Response<NewsModel>
}