package com.kevinhao426.channelninecodetest.di

import android.app.Application
import android.content.Context
import com.kevinhao426.channelninecodetest.api.ApiHelper
import com.kevinhao426.channelninecodetest.api.ApiHelperImpl
import com.kevinhao426.channelninecodetest.api.ApiService
import com.kevinhao426.channelninecodetest.utils.BitmapHelper
import com.kevinhao426.channelninecodetest.view.NewsListAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    @Singleton
    fun bindContext(application: Application): Context = application.applicationContext

    @Singleton
    @Provides
    fun provideOkHttpClient() = OkHttpClient
        .Builder()
        .build()


    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl("https://bruce-v2-mob.fairfaxmedia.com.au")
        .client(okHttpClient)
        .build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun provideApiHelper(apiHelper: ApiHelperImpl): ApiHelper = apiHelper

    @Provides
    @Singleton
    fun provideBitmapHelper(context: Context): BitmapHelper =
        BitmapHelper(context)

    @Provides
    @Singleton
    fun provideNewListAdapter(bitmapHelper: BitmapHelper): NewsListAdapter = NewsListAdapter(bitmapHelper)
}