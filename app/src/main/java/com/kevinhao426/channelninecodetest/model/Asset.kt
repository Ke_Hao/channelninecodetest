package com.kevinhao426.channelninecodetest.model

data class Asset(
    val byLine: String,
    val headline: String,
    val id: Int,
    val relatedImages: List<RelatedImage>,
    val theAbstract: String,
    val timeStamp: Long,
    val url: String
)