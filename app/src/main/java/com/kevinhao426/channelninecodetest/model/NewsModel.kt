package com.kevinhao426.channelninecodetest.model

data class NewsModel(
    val assets: List<Asset>,
    val displayName: String,
    val relatedImages: List<RelatedImage>,
    val timeStamp: Long,
)