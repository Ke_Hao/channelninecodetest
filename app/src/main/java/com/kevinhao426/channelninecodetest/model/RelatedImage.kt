package com.kevinhao426.channelninecodetest.model

data class RelatedImage(
    val height: Int,
    val id: Int,
    val timeStamp: Long,
    val url: String,
    val width: Int
)