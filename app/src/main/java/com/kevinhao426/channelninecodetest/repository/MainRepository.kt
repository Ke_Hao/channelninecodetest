package com.kevinhao426.channelninecodetest.repository

import com.kevinhao426.channelninecodetest.api.ApiHelper
import javax.inject.Inject

class MainRepository @Inject constructor(
    private val apiHelper: ApiHelper
){
    suspend fun getChannelNineNews() = apiHelper.getChannelNineNews()
}