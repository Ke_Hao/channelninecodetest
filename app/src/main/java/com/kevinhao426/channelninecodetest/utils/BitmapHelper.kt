package com.kevinhao426.channelninecodetest.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.collection.LruCache
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.BufferedInputStream
import java.io.FileOutputStream
import java.net.URL
import javax.inject.Inject

/**
 * The bitmap helper class that handles the bitmap loading and two tier caching Async
 *
 * Asynchronous logic based on Coroutine
 *
 * Can be injected in UI tier. Since context is mandatory, using this helper in view model is not recommended
 *
 * Caching includes memory cache and disk cache
 * - memory cache uses LruCache lib that natively provided by Androidx
 * - disk cache saves the bitmap as .jpg file under external cache dir of the app.
 *
 * Running logic: simply call getBitmap() with the image url passed as parameter
 *                - it will first check MEMORY CACHE, return bitmap if exists, else
 *                - check DISK CACHE, return bitmap if file exists (and cache in memory), else
 *                - fetch from server through URL, return bitmap if file received (and cache in memory and save to disk), else
 *                - return null
 *
 * For the key that used for cache and disk, please check UrlToKeyConverter.kt
 */
class BitmapHelper @Inject constructor(
    private val context: Context,
) {
    private val MAX_MEMORY_CACHE_SIZE = 20
    private val lruCache: LruCache<String, Bitmap> = LruCache(MAX_MEMORY_CACHE_SIZE)

    //in IO thread, retrieve bitmap from cache with URL as key first, if failed, fetch it from internet
    suspend fun getBitmap(url: String): Bitmap? {
        return withContext(Dispatchers.IO) {
            retrieveFromMemoryCache(url) ?: retrieveFromDisk(url) ?: fetchBitmapFromInternet(url)
        }
    }

    private suspend fun saveToMemoryCache(url: String, bitmap: Bitmap) {
        withContext(Dispatchers.IO) {
            try {
                lruCache.put(getBitmapURLKey(url), bitmap)
            } catch (e: Exception) {
                Timber.e(e.message)
            }
        }
    }

    private suspend fun retrieveFromMemoryCache(url: String): Bitmap? {
        return withContext(Dispatchers.IO) {
            Timber.d("Bitmap from Memory Cache: $url")
            lruCache.get(getBitmapURLKey(url))
        }
    }

    private suspend fun saveToDisk(url: String, bitmap: Bitmap) {
        withContext(Dispatchers.IO) {
            try {
                val file = getBitmapDiskCache(context, url, ImageFormat.JPG)
                if (!file.exists()) file.createNewFile()

                val fileOutputStream = FileOutputStream(file)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream)
                fileOutputStream.flush()
                fileOutputStream.close()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    private suspend fun retrieveFromDisk(url: String): Bitmap? {
        return withContext(Dispatchers.IO) {
            val file = getBitmapDiskCache(context, url, ImageFormat.JPG)
            if (file.exists()) {
                Timber.d("Bitmap from Disk Cache: $url")
                val bitmap = BitmapFactory.decodeFile(file.absolutePath)
                saveToMemoryCache(url, bitmap)
                bitmap
            } else {
                null
            }
        }
    }

    //in IO thread, fetch the bitmap from internet, if success, save it in memory cache with URL as the key
    private suspend fun fetchBitmapFromInternet(url: String): Bitmap? {
        return withContext(Dispatchers.IO) {
            var stream: BufferedInputStream? = null
            try {
                Timber.d("Bitmap from Server: $url")
                val connection = URL(url).openConnection()
                connection.connect()

                stream = BufferedInputStream(connection.getInputStream())
                val bitmap: Bitmap? = BitmapFactory.decodeStream(stream)

                bitmap?.let {
                    saveToMemoryCache(url, it)
                    saveToDisk(url, it)
                }
                bitmap
            } catch (e: Exception) {
                Timber.e(e.message)
                null
            } finally {
                stream?.let {
                    try {
                        it.close()
                    } catch (e: Exception) {
                        Timber.e(e.message)
                    }
                }
            }
        }
    }
}