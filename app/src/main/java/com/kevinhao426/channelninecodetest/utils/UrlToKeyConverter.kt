package com.kevinhao426.channelninecodetest.utils

import android.content.Context
import android.os.Environment
import java.io.File
import java.security.MessageDigest
import java.util.*

//This is the function to get the disk cache dir for bitmap disk cache. Depending on the device is using SDCard or not, we save in different dir
fun getBitmapDiskCache(context: Context, url: String, format: ImageFormat): File {
    return File(getDiskCachePath(context), getBitmapURLKey(url) + format.endFix)
}

fun getDiskCachePath(context: Context): String =
    if (Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()
        || !Environment.isExternalStorageRemovable()
    ) {
        context.externalCacheDir!!.path
    } else {
        context.cacheDir.path
    }

//a simple function to convert url to a 32 digit int like string, that can be used as the key to for caching
fun getBitmapURLKey(url: String): String {
    return try {
        val digest: MessageDigest = MessageDigest.getInstance("md5")
        digest.update(url.toByteArray())
        val bytes: ByteArray = digest.digest()
        val sb = StringBuilder()
        for (i in bytes.indices) {
            sb.append(String.format("%02X", bytes[i]))
        }
        sb.toString().lowercase(Locale.getDefault())
    } catch (e: Exception) {
        url.hashCode().toString()
    }
}

//define image format that in use.
enum class ImageFormat(val endFix: String) {
    JPG(".jpg")
}
