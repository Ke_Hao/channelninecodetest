package com.kevinhao426.channelninecodetest.view

import android.app.AlertDialog
import android.os.Bundle
import androidx.activity.viewModels
import androidx.annotation.VisibleForTesting
import androidx.appcompat.app.AppCompatActivity
import com.kevinhao426.channelninecodetest.R
import com.kevinhao426.channelninecodetest.utils.BitmapHelper
import com.kevinhao426.channelninecodetest.utils.replaceFragment
import com.kevinhao426.channelninecodetest.viewModel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    val viewModel: MainViewModel by viewModels()

    @Inject
    lateinit var bitmapHelper: BitmapHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initFragment()

        observeEvents()

        viewModel.fetchNewsData()
    }

    private fun observeEvents() {
        updateTitleBar()
        onErrorReceived()
    }

    private fun onErrorReceived() {
        viewModel.errorData.observe(this) {
            AlertDialog.Builder(this)
                .setMessage(it)
                .setPositiveButton(
                    getString(R.string.retry)
                ) { _, _ -> viewModel.fetchNewsData() }
                .setCancelable(false)
                .create()
                .show()
        }
    }

    private fun updateTitleBar(){
        viewModel.newsData.observe(this) {
            supportActionBar?.title = it.displayName
        }
    }

    private fun initFragment() {
        supportFragmentManager.replaceFragment(NewsGridFragment.newInstance())
    }
}