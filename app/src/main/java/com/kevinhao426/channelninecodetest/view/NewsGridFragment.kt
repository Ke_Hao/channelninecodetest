package com.kevinhao426.channelninecodetest.view

import android.content.Intent
import android.os.Bundle
import android.view.DragEvent.ACTION_DRAG_ENDED
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import com.kevinhao426.channelninecodetest.databinding.FragmentNewsGridBinding
import com.kevinhao426.channelninecodetest.viewModel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class NewsGridFragment @Inject constructor() : Fragment() {

    @Inject
    lateinit var newsListAdapter: NewsListAdapter

    private val activityViewModel by activityViewModels<MainViewModel>()

    private lateinit var binding: FragmentNewsGridBinding

    companion object {
        fun newInstance(): NewsGridFragment {
            return NewsGridFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewsGridBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindingUIEvents()
        observeData()
    }

    private fun observeData() {
        onNewsDataUpdate()
        onLoadingStatusUpdate()
    }

    private fun onLoadingStatusUpdate() {
        activityViewModel.isLoading.observe(viewLifecycleOwner) {
            binding.refreshLayout.isRefreshing = it
        }
    }

    private fun onNewsDataUpdate() {
        activityViewModel.newsData.observe(viewLifecycleOwner) {
            val sortedList = it.assets.sortedByDescending { news ->
                news.timeStamp
            }
            newsListAdapter.updateNewsList(sortedList)
        }
    }

    private fun bindingUIEvents() {
        binding.newsListGrid.apply {
            this.layoutManager = LinearLayoutManager(context, VERTICAL, false)
            this.adapter = newsListAdapter.apply {
                onNewsItemClicked = {
                    openURL(it.url, it.headline)
                }
            }
        }

        binding.refreshLayout.setOnRefreshListener {
            activityViewModel.fetchNewsData()
        }
    }

    override fun onResume() {
        super.onResume()
        activityViewModel.newsScrollState.observe(viewLifecycleOwner) {
            binding.newsListGrid.layoutManager?.onRestoreInstanceState(it)
        }
    }

    private fun openURL(fullUrl: String, title: String) {
        val intent = Intent(context, WebViewActivity::class.java)
        intent.putExtra(WebViewActivity.NEWS_WEB_URL, fullUrl)
        intent.putExtra(WebViewActivity.NEWS_TITLE, title)
        startActivity(intent)
    }

    override fun onPause() {
        super.onPause()
        activityViewModel.newsScrollState.value =
            binding.newsListGrid.layoutManager?.onSaveInstanceState()
    }
}