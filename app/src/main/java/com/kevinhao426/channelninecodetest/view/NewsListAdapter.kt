package com.kevinhao426.channelninecodetest.view

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kevinhao426.channelninecodetest.databinding.ItemNewsItemBinding
import com.kevinhao426.channelninecodetest.model.Asset
import com.kevinhao426.channelninecodetest.model.RelatedImage
import com.kevinhao426.channelninecodetest.utils.BitmapHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class NewsListAdapter @Inject constructor(val bitmapHelper: BitmapHelper) :
    RecyclerView.Adapter<NewsListAdapter.NewsItemViewHolder>() {
    private val newsList: MutableList<Asset> = mutableListOf()

    lateinit var context: Context
    lateinit var onNewsItemClicked: (data: Asset) -> Unit

    fun updateNewsList(list: List<Asset>) {
        newsList.clear()
        newsList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsItemViewHolder {
        context = parent.context
        return NewsItemViewHolder(
            ItemNewsItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: NewsItemViewHolder, position: Int) {
        val newsData = newsList[position]
        holder.binding.itemNewsHeadline.text = newsData.headline
        holder.binding.itemNewsAbstract.text = newsData.theAbstract
        holder.binding.itemNewsByline.text = newsData.byLine

        //asynchronously load image through coroutine
        MainScope().launch {
            val bitmap = withContext(Dispatchers.IO) {
                pickSmallestImageUrl(newsData.relatedImages)?.let {
                    bitmapHelper.getBitmap(it)
                }
            }
            holder.binding.itemNewsImage.setImageBitmap(bitmap)
        }

        holder.binding.root.setOnClickListener {
            onNewsItemClicked(newsData)
        }
    }

    override fun getItemCount(): Int = newsList.size

    class NewsItemViewHolder(val binding: ItemNewsItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    private fun pickSmallestImageUrl(list: List<RelatedImage>): String? {
        return list.minByOrNull {
            it.width * it.height
        }?.url
    }
}
