package com.kevinhao426.channelninecodetest.view

import android.os.Bundle
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.kevinhao426.channelninecodetest.databinding.ActivityWebViewBinding

class WebViewActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWebViewBinding

    companion object {
        const val NEWS_WEB_URL = "newsWebUrl"
        const val NEWS_TITLE = "newsTitle"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWebViewBinding.inflate(layoutInflater)
        setContentView(binding.root)
        intent.getStringExtra(NEWS_TITLE)?.let {
            supportActionBar?.title = it
        }

        val webView = binding.webView

        //force load in webview rather than in browser.
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                return !request?.url.toString().startsWith("https://")
            }
        }

        webView.settings.apply {
            javaScriptEnabled = true
            useWideViewPort = true
            loadWithOverviewMode = true
        }

        intent.getStringExtra(NEWS_WEB_URL)?.let {
            webView.loadUrl(it)
        }
    }
}