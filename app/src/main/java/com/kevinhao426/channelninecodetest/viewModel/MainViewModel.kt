package com.kevinhao426.channelninecodetest.viewModel

import android.os.Parcelable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kevinhao426.channelninecodetest.model.NewsModel
import com.kevinhao426.channelninecodetest.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val mainRepository: MainRepository,
) : ViewModel() {
    val newsData = MutableLiveData<NewsModel>()
    val errorData = MutableLiveData<String>()
    val isLoading = MutableLiveData<Boolean>()

    val newsScrollState = MutableLiveData<Parcelable>()

    fun fetchNewsData() = viewModelScope.launch {
        isLoading.postValue(true)
        mainRepository.getChannelNineNews().let {
            if (it.isSuccessful) {
                newsData.postValue(it.body())
            } else {
                errorData.postValue(it.errorBody().toString())
            }
            isLoading.postValue(false)
        }
    }
}