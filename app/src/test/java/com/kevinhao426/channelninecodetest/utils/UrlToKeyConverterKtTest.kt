package com.kevinhao426.channelninecodetest.utils

import junit.framework.TestCase
import org.junit.Test

class UrlToKeyConverterKtTest : TestCase(){
    @Test
    fun testUrlToKeyConverter(){
        assertEquals("a5e930afcd16cfa96d08f093637d99e7", getBitmapURLKey("https://www.fairfaxstatic.com.au/content/dam/images/g/t/o/z/7/r/image.related.landscape.1174x783.p59qhb.13zzqx.png/1643015957204.jpg"))
    }
}