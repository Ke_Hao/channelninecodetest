# The demo project for Channel Nine. 

## Implememtation: 
 * A single activity application with MVVM pattern
 * Async task handled by Coroutine
 * Api call handled by Retrofit
 * Two tier bitmap caching: Memory cache and Disk saving
 * Memory cache uses LruCache 
 * Disk cache uses IO file processing
 * The App is using Hilt for Depencency Injection
 * Timber for logging at dev level
 
## User flow: 
- Run the app on device, with MainActivity onCreate, the app will request from and observe to the API Endpoint.

- If API returns an error, pop up an alert dialog. By clicking the dialog button, dialog dismissed then request API again.

- If API returns an success, update "newsData" in viewmodel, which: 
- 1. update application title bar text: 
- 2. update the adapter data set in fragment. 

- The fragment will display the news in the order as requested. 

- If the news item data contains image url, the fragment will get the bitmap as following logic: 
- 1. get the bitmap from Memory Cache, if exist, return bitmap and display; if not exist: 
- 2. get the bitmap from Disk Saving, is exist, cache bitmap into Memory Cache, then return and display; if not exist: 
- 3. get the bitmap from Server by the URL, if returns, compress and save the bitmap to disk, cache bitmap into Memory Cache, then return and display; if not responded from server, return null.
- 4. all Disk Saving and Memory caching are using a 32 digit hash string that based on the url and md5 encryption. 
- 5. if the flow returns Null for bitmap, the ImageView will display nothing. 

- In Fragment, swipe down the list, the app will call API to update list again. 
- If rotate the screen: the fragment will switch to landscape / potrait display, then scroll to the last read position.
- Click the news item in list, will open an internal webview for displaying the news


## Requirements as follows: 

## Task



The included API endpoint returns, amongst other data, an array of news stories (assets).

You are tasked with creating a Phone app that consumes the provided API and displays a list of news articles to the user, ideally in a RecycleView or GridView. Tapping a story should present the asset’s URL in a WebView (or any other better alternate)



## Requirements



* The list of articles should display at least the following fields:

-- headline

-- theAbstract

-- byLine



* Display latest article first in the list, use article's 'timeStamp'



* If there are related images available for an asset, display the smallest image available for the asset in the cell.

* Images should be loaded asynchronously and cached



* The style of cells is up to you, with necessary padding and layout.

* Use activity and/or fragments where appropriate, but should be adaptable to all Phone screen sizes/rotation.



* Comment your code so it can be understood in six months



* A good unit test coverage is expected as part of solution

* Put some Espresso / Instrument UI tests to verify UI is functional and/or cover some user flow



* Use Android Studio 4 (stable) and Kotlin language, please specify code compilation notes in your submission.



Please feel free to ask if you have any questions when interpreting this document!



## Submission Notes



* Code Compilation instructions; IDE/Plugin versions expected, dependency management

* Short description explaining architecture and logical modules its comprised of (e.g View, ViewModel...)

* Any 3rd party libraries used and rational

* Explain what each test does in comments or in document format

* Any additional features -- apart the requirements given above



* Please either send us solution in zip file or share link to your cloud version control, include above notes in submission.





## How we evaluate



We want you to succeed! We aim to evaluate each submission with the same criteria, they are:



 * *requirements* you've build the right product, attention to details!

 * *code architecture* appropriate design patterns used (MVVM, MVP ...)

 * *code style* idiomatic, safe, clean, concise.

 * *unit tests* coverage, stable, reliable, maintainable, mocked where required
 
 * *ui tests* instrumented test for simulating user interactions 

 * *user experience* responsive, user-centric design. support orientation change.


## Resources



API Endpoint:

https://bruce-v2-mob.fairfaxmedia.com.au/1/coding_test/13ZZQX/full